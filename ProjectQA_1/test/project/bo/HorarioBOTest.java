/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.bo;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import project.dao.HorarioDAO;
import project.entities.Horario;

/**
 *
 * @author Josue
 */
public class HorarioBOTest {

    private HorarioDAO hdao = new HorarioDAO();

    public HorarioBOTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

//-------------------------------------------------------------------------------------------------------------------------------------------------------
    //test registrar, el cual ingresara datos validos a la base de datos
    @Test
    public void testRegistrar() {
        Horario h = horario1();
        HorarioBO instance = new HorarioBO(hdao);
        boolean expResult = true;
        boolean result = instance.registrar(h);
        assertEquals(expResult, result);
        System.out.println("Registro realizado");
    }
    //test registrar el cual al mostrar datos no validos, este no registrara ningun dato.
    @Test
    public void testRegistrar2() {
        Horario h = horario2();
        HorarioBO instance = new HorarioBO(hdao);
        boolean expResult = true;
        boolean result = instance.registrar(h);
        assertEquals(expResult, result);
        System.out.println("Registro realizado");
    }
    //test registrar 
    @Test
    public void testRegistrar3() {
        Horario h = horario3();
        HorarioBO instance = new HorarioBO(hdao);
        boolean expResult = true;
        boolean result = instance.registrar(h);
        assertEquals(expResult, result);
        System.out.println("Registro realizado");
    }

    //Registro de horario con los campos debidamente completados
    public Horario horario1() {
        Horario h = new Horario();
        h.setNum_bus(10);
        h.setDescrip_horario("Es un bus color azul");
        h.setSalida("03:20");
        h.setLlegada("04:10");
        h.setEstado(true);
        return h;
    }

    //Registro de horario con datos incompletos
    public Horario horario2() {
        Horario h = new Horario();
        h.setNum_bus(9);
        h.setDescrip_horario("");
        h.setSalida("03:20");
        h.setLlegada("");
        h.setEstado(true);
        return h;
    }

    //Registro de numero de bus ya registrado
    public Horario horario3() {
        //Este paso debe de verificar que dentro de la
        //base de datos ya debe de estar el bus numero 1
        Horario h = new Horario();
        h.setNum_bus(1);
        h.setDescrip_horario("12");
        h.setSalida("012");
        h.setLlegada("12");
        h.setEstado(true);
        return h;
    }
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Test of cargarCod method, of class HorarioBO.
     */
    //Realizar búsqueda de horarios según un código de horario ya existente. 
    @Test
    public void testCargarCod1() {
        HorarioBO instance = new HorarioBO(hdao);
        int codigo = 15;
        boolean expResult = true;
        boolean result = instance.cargarCod(1) != null;
        assertEquals(expResult, result);
        System.out.println(result);
    }

    //Realizar búsqueda de horarios según un código no existente. 
    @Test
    public void testCargarCod2() {
        HorarioBO instance = new HorarioBO(hdao);
        boolean expResult = true;
        boolean result = instance.cargarCod(0) != null;
        assertEquals(expResult, result);
        System.out.println(result);
    }

    //Realizar búsqueda de horarios con espacios al inicio y al final del código. 
    @Test
    public void testCargarCod3() {
        HorarioBO instance = new HorarioBO(hdao);
        int codigo = 15;
        boolean expResult = true;
        boolean result = instance.cargarCod(15) != null;
        assertEquals(expResult, result);
        System.out.println(result);
    }
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------

    @Test
    public void testModificar1() {
        HorarioBO instance = new HorarioBO(hdao);
        boolean expResult = true;
        boolean result = instance.modificar(18, horariom1());
        assertEquals(expResult, result);
    }

    @Test
    public void testModificar2() {
        HorarioBO instance = new HorarioBO(hdao);
        boolean expResult = true;
        boolean result = instance.modificar(18, horariom2());
        assertEquals(expResult, result);
    }

    @Test
    public void testModifica3() {
        HorarioBO instance = new HorarioBO(hdao);
        boolean expResult = true;
        boolean result = instance.modificar(18, horariom3());
        assertEquals(expResult, result);
    }

    public Horario horariom1() {

        Horario h = new Horario();
        h.setNum_bus(10);
        h.setDescrip_horario("Es un bus color verde");
        h.setSalida("01:20");
        h.setLlegada("11:10");
        h.setEstado(true);
        return h;
    }

    public Horario horariom2() {

        Horario h = new Horario();
        h.setNum_bus(10);
        h.setDescrip_horario("Es un bus color verde");
        h.setSalida("01 20");
        h.setLlegada("11 10");
        h.setEstado(true);
        return h;
    }

    public Horario horariom3() {

        Horario h = new Horario();
        h.setNum_bus(10);
        h.setDescrip_horario(" ");
        h.setSalida("01:20");
        h.setLlegada("11:10");
        h.setEstado(true);
        return h;
    }
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------

    @Test
    public void testCambiarEstado() {
        HorarioBO instance = new HorarioBO(hdao);
        int codigo = 1;
        boolean expResult = true;
        boolean result = instance.cambiarEstado(1);
        assertEquals(expResult, result);
        System.out.println("Eliminado exitosamente");
    }

    @Test
    public void testCambiarEstado2() {
        HorarioBO instance = new HorarioBO(hdao);
        int codigo = 15;
        boolean expResult = true;
        boolean result = instance.cambiarEstado(15);
        assertEquals(expResult, result);
        System.out.println("Eliminado exitosamente");
    }

    @Test
    public void testCambiarEstado3() {
        HorarioBO instance = new HorarioBO(hdao);
        int codigo = 0;
        boolean expResult = true;
        boolean result = instance.cambiarEstado(0);
        assertEquals(expResult, result);
        System.out.println("Eliminado exitosamente");
    }
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------

    @Test
    public void testValidarHora() {
        System.out.println("validarHora");
        String value = "";
        HorarioBO instance = null;
        String expResult = "";
        String result = instance.validarHora(value);
        assertEquals(expResult, result);
    }
//--------------------------------------------------------------------------------------------------------------------------------------------------------------
    @Test
    public void testCargarRepor1() {
        HorarioBO instance = new HorarioBO(hdao);
        int codigo = 15;
        boolean expResult = true;
        boolean result = instance.cargarCod(1) != null;
        assertEquals(expResult, result);
        System.out.println(result);
    }

    @Test
    public void testCargarReport2() {
        HorarioBO instance = new HorarioBO(hdao);
        boolean expResult = true;
        boolean result = instance.cargarCod(1000) != null;
        assertEquals(expResult, result);
        System.out.println(result);
    }

    @Test
    public void testCargarReport3() {
        HorarioBO instance = new HorarioBO(hdao);
        int codigo = 0;
        boolean expResult = true;
        boolean result = instance.cargarCod(0) != null;
        assertEquals(expResult, result);
        System.out.println(result);
    }
//--------------------------------------------------------------------------------------------------------------------------------------------------------------
}

