/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.bo;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import project.dao.RecorridoDAO;
import project.entities.Recorrido;

/**
 *
 * @author Josue
 */
public class RecorridoBOTest {

    private RecorridoDAO hdao = new RecorridoDAO();

    public RecorridoBOTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of registrar method, of class RecorridoBO.
     */
//-------------------------------------------------------------------------------------------------------------------------------------------------------
    //test registrar, el cual ingresara datos validos a la base de datos
    @Test
    public void testRegistrar() {
        Recorrido r = recorrido1();
        RecorridoBO instance = new RecorridoBO(hdao);
        boolean expResult = true;
        boolean result = instance.registrar(r);
        assertEquals(expResult, result);
        System.out.println("Registro recorrido realizado");
    }

    //test registrar el cual al mostrar datos no validos, este no registrara ningun dato.
    @Test
    public void testRegistrar2() {
        Recorrido r = recorrido2();
        RecorridoBO instance = new RecorridoBO(hdao);
        boolean expResult = true;
        boolean result = instance.registrar(r);
        assertEquals(expResult, result);
        System.out.println("Registro recorrido realizado");
    }

    //test registrar 
    @Test
    public void testRegistrar3() {
        Recorrido r = recorrido3();
        RecorridoBO instance = new RecorridoBO(hdao);
        boolean expResult = true;
        boolean result = instance.registrar(r);
        assertEquals(expResult, result);
        System.out.println("Registro recorrido realizado");
    }

    //Registro de recorrido con los campos debidamente completados
    public Recorrido recorrido1() {
        Recorrido r = new Recorrido();
        r.setNum_bus(12);
        r.setDescripcion("Bus de transporte escolar");
        r.setTiem_demora("2:00");
        r.setZona_recorrido("Ciudad Quesada - San Ramon");
        r.setCant_recorrido(4);
        r.setEstado(true);
        return r;
    }

    //Registro de recorrido con datos incompletos
    public Recorrido recorrido2() {
        Recorrido r = new Recorrido();
        r.setNum_bus(2);
        r.setDescripcion("");
        r.setTiem_demora("2:00");
        r.setZona_recorrido("");
        r.setCant_recorrido(4);
        r.setEstado(true);
        return r;
    }

    //Registro de numero de recorrido ya registrado
    public Recorrido recorrido3() {
        Recorrido r = new Recorrido();
        r.setNum_bus(2);
        r.setDescripcion("!!@@!@@!@!@");
        r.setTiem_demora("!@##!#!");
        r.setZona_recorrido("$@#@#@----#@#@#");
        r.setCant_recorrido(4);
        r.setEstado(true);
        return r;
    }

//--------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //Realizar búsqueda de horarios según un código de horario ya existente. 
    @Test
    public void testCargarCod1() {
        RecorridoBO instance = new RecorridoBO(hdao);
        int codigo = 1;
        boolean expResult = true;
        boolean result = instance.cargarCod(2) != null;
        assertEquals(expResult, result);
        System.out.println("Listado de del codigo " + codigo);

    }

    //Realizar búsqueda de horarios según un código no existente. 
    @Test
    public void testCargarCod2() {
        RecorridoBO instance = new RecorridoBO(hdao);
        boolean expResult = false;
        boolean result = instance.cargarCod(100) != null;
        assertEquals(expResult, result);
        System.out.println(result);
    }

    //Realizar búsqueda de horarios con espacios al inicio y al final del código. 
    @Test
    public void testCargarCod3() {
        RecorridoBO instance = new RecorridoBO(hdao);
        int codigo = 1;
        boolean expResult = true;
        boolean result = instance.cargarCod(             1             ) != null;
        assertEquals(expResult, result);
        System.out.println(result);
    }
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------

    @Test
    public void testModificar1() {
        RecorridoBO instance = new RecorridoBO(hdao);
        boolean expResult = true;
        boolean result = instance.modificar(3, recorridom1());
        assertEquals(expResult, result);
        System.out.println("Modificacion exitosa");
    }

    @Test
    public void testModificar2() {
        RecorridoBO instance = new RecorridoBO(hdao);
        boolean expResult = true;
        boolean result = instance.modificar(3, recorridom2());
        assertEquals(expResult, result);
    }

    @Test
    public void testModifica3() {
        RecorridoBO instance = new RecorridoBO(hdao);
        boolean expResult = true;
        boolean result = instance.modificar(2, recorridom3());
        assertEquals(expResult, result);
    }

    public Recorrido recorridom1() {
        //Modificacion basica 
        Recorrido r = new Recorrido();
        r.setNum_bus(10);
        r.setDescripcion("Bus de transporte escolar");
        r.setTiem_demora("5:00");
        r.setZona_recorrido("Ciudad Quesada - San Ramon");
        r.setCant_recorrido(4);
        r.setEstado(true);
        return r;
    }

    public Recorrido recorridom2() {
        //No deberia de cambiar o modificar el recorrido estando en estado false.(ELIMINADO)
        Recorrido r = new Recorrido();
        r.setNum_bus(1);
        r.setDescripcion("bus de transporte escolar");
        r.setTiem_demora("2:00");
        r.setZona_recorrido("Ciudad Quesada - San Ramon");
        r.setCant_recorrido(4);
        r.setEstado(true);
        return r;
    }

    public Recorrido recorridom3() {
        //Espacios vacios
        Recorrido r = new Recorrido();
        r.setNum_bus(1);
        r.setDescripcion("           ");
        r.setTiem_demora("");
        r.setZona_recorrido(" ");
        r.setCant_recorrido(4);
        r.setEstado(true);
        return r;
    }
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------

    @Test
    public void testCambiarEstado() {
        RecorridoBO instance = new RecorridoBO(hdao);
        boolean expResult = true;
        boolean result = instance.cambiarEstado(2);
        assertEquals(expResult, result);
        System.out.println("Eliminado exitosamente");
    }

    @Test
    public void testCambiarEstado2() {
        RecorridoBO instance = new RecorridoBO(hdao);
        int codigo = 15;
        boolean expResult = true;
        boolean result = instance.cambiarEstado(1);
        assertEquals(expResult, result);
        System.out.println("Eliminado exitosamente");
    }
    //codigo no existente en la busqueda de eliminacion
    @Test
    public void testCambiarEstado3() {
        RecorridoBO instance = new RecorridoBO(hdao);
        int codigo = 0;
        boolean expResult = true;
        boolean result = instance.cambiarEstado(0);
        assertEquals(expResult, result);
        System.out.println("Eliminado exitosamente");
    }
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------

    @Test
    public void testValidarHora() {
        System.out.println("validarHora");
        String value = "";
        HorarioBO instance = null;
        String expResult = "";
        String result = instance.validarHora(value);
        assertEquals(expResult, result);
    }
//--------------------------------------------------------------------------------------------------------------------------------------------------------------

    @Test
    public void testCargarRepor1() {
        RecorridoBO instance = new RecorridoBO(hdao);
        int codigo = 2;
        boolean expResult = true;
        boolean result = instance.cargarCod(2) != null;
        assertEquals(expResult, result);
        System.out.println(result);
    }

    @Test
    public void testCargarReport2() {
        RecorridoBO instance = new RecorridoBO(hdao);
        boolean expResult = true;
        boolean result = instance.cargarCod(1000) != null;
        assertEquals(expResult, result);
        System.out.println(result);
    }

    @Test
    public void testCargarReport3() {
        RecorridoBO instance = new RecorridoBO(hdao);
        int codigo = 0;
        boolean expResult = true;
        boolean result = instance.cargarCod(                 0                 ) != null;
        assertEquals(expResult, result);
        System.out.println(result);
    }

}
