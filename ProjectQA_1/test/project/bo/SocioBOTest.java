/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.bo;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import project.dao.SocioDAO;
import project.entities.Recorrido;
import project.entities.Socio;

/**
 *
 * @author Josue
 */
public class SocioBOTest {

    private SocioDAO hdao = new SocioDAO();

    public SocioBOTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of registrar method, of class SocioBO.
     */
//-------------------------------------------------------------------------------------------------------------------------------------------------------
    //test registrar, el cual ingresara datos validos a la base de datos
    @Test
    public void testRegistrarS1() {
        Socio s = Socio1();
        SocioBO instance = new SocioBO(hdao);
        boolean expResult = true;
        boolean result = instance.registrar(s);
        assertEquals(expResult, result);
        System.out.println("Registro recorrido realizado");
    }

    //test registrar el cual al mostrar datos no validos, este no registrara ningun dato.
    @Test
    public void testRegistrarS2() {
        Socio s = Socio2();
        SocioBO instance = new SocioBO(hdao);
        boolean expResult = true;
        boolean result = instance.registrar(s);
        assertEquals(expResult, result);
        System.out.println("Registro recorrido realizado");
    }

    //test registrar 
    @Test
    public void testRegistrarS3() {
        Socio s = Socio3();
        SocioBO instance = new SocioBO(hdao);
        boolean expResult = true;
        boolean result = instance.registrar(s);
        assertEquals(expResult, result);
        System.out.println("Registro recorrido realizado");
    }

    //Registro de recorrido con los campos debidamente completados
    public Socio Socio1() {
        Socio s = new Socio();
        s.setNombre("Josué");
        s.setApellido("Martínez");
        s.setLinea_socio("Coopatrack");
        s.setNum_bus(-1);
        s.setCapacidad_bus(54);
        s.setAyudante("David");
        s.setChofer("Gabriel");
        s.setEstado(true);
        return s;
    }
    //Registro de recorrido con datos incompletos
    public Socio Socio2() {
        Socio s = new Socio();
        s.setNombre("Josué");
        s.setApellido("Martínez");
        s.setLinea_socio("Coopatrack");
        s.setNum_bus(1);
        s.setCapacidad_bus(54);
        s.setAyudante("David");
        s.setChofer("Gabriel");
        s.setEstado(true);
        return s;
    }

    //Registro de espacios vaciso en el registr  de socio 
    public Socio Socio3() {
        Socio s = new Socio();
        s.setNombre("");
        s.setApellido("");
        s.setLinea_socio("");
        s.setNum_bus(1);
        s.setCapacidad_bus(54);
        s.setAyudante("");
        s.setChofer("");
        s.setEstado(true);
        return s;
    }

//--------------------------------------------------------------------------------------------------------------------------------------------------------------------
}
