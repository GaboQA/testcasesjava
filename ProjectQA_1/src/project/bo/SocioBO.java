/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.bo;

import project.dao.SocioDAO;
import project.entities.MiError;
import project.entities.Socio;

/**
 *
 * @author Josue
 */
public class SocioBO {
    
    private final SocioDAO Sdao;

    public SocioBO(SocioDAO Sdao) {
        this.Sdao = Sdao;
    }
    
    public boolean registrar(Socio s) {
        if (s.getNombre().isEmpty()) {
            throw new MiError("El nombre del socio es requerido");
        }
        if (s.getApellido().isEmpty()) {
            throw new MiError("El apellido del socio es requerido");
        }
        if (s.getLinea_socio().isEmpty()) {
            throw new MiError("La linea del socio es requerida");
        }
        if (s.getNum_bus() == 0) {
            throw new MiError("El número de bus requerido");
        }
        if (!verificarBus(s.getNum_bus())) {
            throw new MiError("El número de bus ya existe en el registro");
        }
        if (s.getCapacidad_bus() < 0) {
            throw new MiError("La capacidad del bus no debe ser menor a 0");
        }
        if (s.getAyudante().isEmpty()) {
            throw new MiError("El nombre del ayudante es requerido");
        }
        if (s.getChofer().isEmpty()) {
            throw new MiError("El nombre del chofer es requerido");
        }
        if (!s.isEstado()) {
            throw new MiError("Debe seleccionar el espacio estado");
        }

        SocioDAO sdao = new SocioDAO();
        return sdao.insertar(s);

    }
    
    private boolean verificarBus(int bus) {
        SocioDAO sdao = new SocioDAO();
        return sdao.cargarHorarioN(bus) == null;
    }
}
