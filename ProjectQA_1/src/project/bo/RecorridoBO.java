/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.bo;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import project.dao.RecorridoDAO;
import project.entities.MiError;
import project.entities.Recorrido;

/**
 *
 * @author Josue
 */
public class RecorridoBO {
    
    private RecorridoDAO Rdao;

    public RecorridoBO(RecorridoDAO Rdao) {
        this.Rdao = Rdao;
    }
    
    public boolean registrar(Recorrido r) {
        
        if (r.getNum_bus() == 0) {
            throw new MiError("El número de bus requerido");
        }
        if (!verificarBus(r.getNum_bus())) {
            throw new MiError("El número de bus ya existe en el registro");
        }
        if (r.getDescripcion().isEmpty()) {
            throw new MiError("Descripción del recorrido es requerida");
        }
        if (validarHora(r.getTiem_demora()).isEmpty()) {
            throw new MiError("El formato de la hora para el campo tiempo de demora no es el adecuado");
        }
        if (r.getCant_recorrido() < 0) {
            throw new MiError("La cantidad de rrecorrido no debe ser menor a 0");
        }
        if (!r.isEstado()) {
            throw new MiError("Debe seleccionar el espacio estado");
        }

        RecorridoDAO rdao = new RecorridoDAO();
        return rdao.insertar(r);

    }
    
    public boolean modificar(int code, Recorrido recorrido) {
        Recorrido r = cargarCod(code);
        recorrido.setCod_recorrido(r.getCod_recorrido());
        
        if (r.getNum_bus() == 0) {
            throw new MiError("El número de bus requerido");
        }
        if (r.getDescripcion().isEmpty()) {
            throw new MiError("Descripción del recorrido es requerida");
        }
        if (validarHora(r.getTiem_demora()).isEmpty()) {
            throw new MiError("El formato de la hora para el campo tiempo de demora no es el adecuado");
        }
        if (r.getCant_recorrido() < 0) {
            throw new MiError("La cantidad de rrecorrido no debe ser menor a 0");
        }
        if (!r.isEstado()) {
            throw new MiError("Debe seleccionar el espacio estado");
        }

        RecorridoDAO rdao = new RecorridoDAO();
        return rdao.modificar(recorrido);

    }
    
    public boolean cambiarEstado(int code) {
        Recorrido r = cargarCod(code);
        if (r.getCod_recorrido() <= 0) {
            throw new MiError("Favor seleccionar un recorrido valido");
        }
        RecorridoDAO rdao = new RecorridoDAO();
        return rdao.cambiarEstado(r);
    }
    
    public Recorrido cargarCod(int code) {
        RecorridoDAO rdao = new RecorridoDAO();
        if (code <= 0) {
            throw new MiError("Favor seleccionar un recorrido valido");
        }
        if (rdao.cargarRecorrido(code) == null) {
             throw new MiError("El recorrido que busca no existe");
        }
        if (rdao.cargarRecorrido(code).getCod_recorrido() != code) {
             throw new MiError("El recorrido que busca no existe o no es valido");
        }
        return rdao.cargarRecorrido(code);
    }
    
    public String validarHora(String value) {   
        try {
            DateFormat hora = new SimpleDateFormat("HH:mm");
	    Date convertido = hora.parse(value);
            return hora.format(convertido);
        } catch (ParseException e) {
            System.out.println("Error: " + e.getMessage());
        } 
        return "";
    }
    
    private boolean verificarBus(int bus) {
        RecorridoDAO rdao = new RecorridoDAO();
        return rdao.cargarRecorridoN(bus) == null;
    }
    
}
