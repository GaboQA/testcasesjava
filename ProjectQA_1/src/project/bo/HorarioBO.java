/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.bo;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import project.dao.HorarioDAO;
import project.entities.Horario;
import project.entities.MiError;

/**
 *
 * @author Josue
 */
public class HorarioBO {
    
    private final HorarioDAO Hdao;

    public HorarioBO(HorarioDAO Hdao) {
        this.Hdao = Hdao;
    }
    
    public boolean registrar(Horario h) {
        
        if (h.getNum_bus() == 0) {
            throw new MiError("El número de bus requerido");
        }
        if (!verificarBus(h.getNum_bus())) {
            throw new MiError("El número de bus ya existe en el registro");
        }
        if (h.getDescrip_horario().isEmpty()) {
            throw new MiError("Descripción del horario es requerida");
        }
        if (validarHora(h.getSalida()).isEmpty()) {
            throw new MiError("El formato de la hora no es el adecuado");
        }
        if (validarHora(h.getLlegada()).isEmpty()) {
            throw new MiError("El formato de la hora no es el adecuado");
        }
        if (!h.isEstado()) {
            throw new MiError("Debe seleccionar el espacio estado");
        }

        HorarioDAO hdao = new HorarioDAO();
        return hdao.insertar(h);

    }
    
    public boolean modificar(int code, Horario horario) {
        Horario h = cargarCod(code);
        horario.setCod_horario(h.getCod_horario());

        if (h.getCod_horario() == 0) {
            throw new MiError("El horario no existe en el registro");
        }
        if (h.getNum_bus() == 0) {
            throw new MiError("El número de bus requerido");
        }
        if (h.getDescrip_horario().isEmpty()) {
            throw new MiError("Descripción del horario es requerida");
        }
        if (validarHora(h.getSalida()).isEmpty()) {
            throw new MiError("El formato de la hora no es el adecuado");
        }
        if (validarHora(h.getLlegada()).isEmpty()) {
            throw new MiError("El formato de la hora no es el adecuado");
        }
        if (!h.isEstado()) {
            throw new MiError("Debe seleccionar el espacio estado");
        }

        HorarioDAO hdao = new HorarioDAO();
        return hdao.modificar(horario);

    }
    
    public String validarHora(String value) {
        
        try {
            DateFormat hora = new SimpleDateFormat("HH:mm");
	    Date convertido = hora.parse(value);
            return hora.format(convertido);
        } catch (ParseException e) {
            System.out.println("Error: " + e.getMessage());
        } 
        return "";
    }
    
    public boolean cambiarEstado(int code) {
        Horario h = cargarCod(code);
        if (h.getCod_horario() <= 0) {
            throw new MiError("Favor seleccionar un horario valido");
        }
        HorarioDAO hdao = new HorarioDAO();
        return hdao.cambiarEstado(h);
    }
    
    public Horario cargarCod(int code) {
        HorarioDAO hdao = new HorarioDAO();
        if (code <= 0) {
            throw new MiError("Favor seleccionar un horario valido");
        }
        if (hdao.cargarHorario(code) == null) {
             throw new MiError("El horario que busca no existe");
        }
        if (hdao.cargarHorario(code).getCod_horario() != code) {
             throw new MiError("El horario que busca no existe o no es valido");
        }
        return hdao.cargarHorario(code);
    }
     
    private boolean verificarBus(int bus) {
        HorarioDAO hdao = new HorarioDAO();
        return hdao.cargarHorarioN(bus) == null;
    }

}
