/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.entities;

/**
 *
 * @author Josue
 */
public class Socio {
    
    private int cod_socio; 
    private String nombre;
    private String apellido;    
    private String linea_socio; 
    private int num_bus; 
    private int capacidad_bus; 
    private String ayudante;
    private String chofer;   
    private boolean estado;

    public Socio() {
    }

    public Socio(int cod_socio, String nombre, String apellido, String linea_socio, int num_bus, int capacidad_bus, String ayudante, String chofer, boolean estado) {
        this.cod_socio = cod_socio;
        this.nombre = nombre;
        this.apellido = apellido;
        this.linea_socio = linea_socio;
        this.num_bus = num_bus;
        this.capacidad_bus = capacidad_bus;
        this.ayudante = ayudante;
        this.chofer = chofer;
        this.estado = estado;
    }

    public int getCod_socio() {
        return cod_socio;
    }

    public void setCod_socio(int cod_socio) {
        this.cod_socio = cod_socio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getLinea_socio() {
        return linea_socio;
    }

    public void setLinea_socio(String linea_socio) {
        this.linea_socio = linea_socio;
    }

    public int getNum_bus() {
        return num_bus;
    }

    public void setNum_bus(int num_bus) {
        this.num_bus = num_bus;
    }

    public int getCapacidad_bus() {
        return capacidad_bus;
    }

    public void setCapacidad_bus(int capacidad_bus) {
        this.capacidad_bus = capacidad_bus;
    }

    public String getAyudante() {
        return ayudante;
    }

    public void setAyudante(String ayudante) {
        this.ayudante = ayudante;
    }

    public String getChofer() {
        return chofer;
    }

    public void setChofer(String chofer) {
        this.chofer = chofer;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Socio{" + "cod_socio=" + cod_socio + ", nombre=" + nombre + ", apellido=" + apellido + ", linea_socio=" + linea_socio + ", num_bus=" + num_bus + ", capacidad_bus=" + capacidad_bus + ", ayudante=" + ayudante + ", chofer=" + chofer + ", estado=" + estado + '}';
    }
    
}
