/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.entities;

/**
 *
 * @author Josue
 */
public class Horario {
    
    private int cod_horario;    
    private int num_bus;
    private String descrip_horario;
    private String salida;    
    private String llegada;    
    private boolean estado;

    public Horario() {
    }

    public Horario(int cod_horario, int num_bus, String descrip_horario, String salida, String llegada, boolean estado) {
        this.cod_horario = cod_horario;
        this.num_bus = num_bus;
        this.descrip_horario = descrip_horario;
        this.salida = salida;
        this.llegada = llegada;
        this.estado = estado;
    }

    public int getCod_horario() {
        return cod_horario;
    }

    public void setCod_horario(int cod_horario) {
        this.cod_horario = cod_horario;
    }

    public int getNum_bus() {
        return num_bus;
    }

    public void setNum_bus(int num_bus) {
        this.num_bus = num_bus;
    }

    public String getDescrip_horario() {
        return descrip_horario;
    }

    public void setDescrip_horario(String descrip_horario) {
        this.descrip_horario = descrip_horario;
    }

    public String getSalida() {
        return salida;
    }

    public void setSalida(String salida) {
        this.salida = salida;
    }

    public String getLlegada() {
        return llegada;
    }

    public void setLlegada(String llegada) {
        this.llegada = llegada;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Horario{" + "cod_horario=" + cod_horario + ", num_bus=" + num_bus + ", descrip_horario=" + descrip_horario + ", salida=" + salida + ", llegada=" + llegada + ", estado=" + estado + '}';
    }
    
}
