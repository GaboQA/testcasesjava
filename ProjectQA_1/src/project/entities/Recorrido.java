/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.entities;

/**
 *
 * @author Josue
 */
public class Recorrido {
    
    private int cod_recorrido; 
    private int num_bus;
    private String descripcion;
    private String tiem_demora;    
    private String zona_recorrido;  
    private int cant_recorrido; 
    private boolean estado;

    public Recorrido() {
    }

    public Recorrido(int cod_recorrido, int num_bus, String descripcion, String tiem_demora, String zona_recorrido, int cant_recorrido, boolean estado) {
        this.cod_recorrido = cod_recorrido;
        this.num_bus = num_bus;
        this.descripcion = descripcion;
        this.tiem_demora = tiem_demora;
        this.zona_recorrido = zona_recorrido;
        this.cant_recorrido = cant_recorrido;
        this.estado = estado;
    }

    public int getCod_recorrido() {
        return cod_recorrido;
    }

    public void setCod_recorrido(int cod_recorrido) {
        this.cod_recorrido = cod_recorrido;
    }

    public int getNum_bus() {
        return num_bus;
    }

    public void setNum_bus(int num_bus) {
        this.num_bus = num_bus;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTiem_demora() {
        return tiem_demora;
    }

    public void setTiem_demora(String tiem_demora) {
        this.tiem_demora = tiem_demora;
    }

    public String getZona_recorrido() {
        return zona_recorrido;
    }

    public void setZona_recorrido(String zona_recorrido) {
        this.zona_recorrido = zona_recorrido;
    }

    public int getCant_recorrido() {
        return cant_recorrido;
    }

    public void setCant_recorrido(int cant_recorrido) {
        this.cant_recorrido = cant_recorrido;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Recorrido{" + "cod_recorrido=" + cod_recorrido + ", num_bus=" + num_bus + ", descripcion=" + descripcion + ", tiem_demora=" + tiem_demora + ", zona_recorrido=" + zona_recorrido + ", cant_recorrido=" + cant_recorrido + ", estado=" + estado + '}';
    }
    
}
