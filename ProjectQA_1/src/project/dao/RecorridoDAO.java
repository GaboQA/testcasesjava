/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import project.entities.MiError;
import project.entities.Recorrido;

/**
 *
 * @author Josue
 */
public class RecorridoDAO {
    
    public boolean insertar(Recorrido r) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "insert into recorridos(num_bus, descripcion, tiem_demora, zona_recorrido, cant_recorrido, estado) "
                    + "values (?,?,?,?,?,?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, r.getNum_bus());
            stmt.setString(2, r.getDescripcion());
            stmt.setString(3, r.getTiem_demora());
            stmt.setString(4, r.getZona_recorrido());
            stmt.setInt(5, r.getCant_recorrido());
            stmt.setBoolean(6, r.isEstado());
            return stmt.executeUpdate() > 0;
        } catch (Exception ex) {
            throw new MiError("No se pudo registrar el Recorrido, favor intente nuevamente");
        }
    }
    
    public boolean modificar(Recorrido r) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "update recorridos set num_bus=?, descripcion=?, tiem_demora=?, zona_recorrido=?, cant_recorrido=?, estado=?"
                    + " where cod_recorrido=?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, r.getNum_bus());
            stmt.setString(2, r.getDescripcion());
            stmt.setString(3, r.getTiem_demora());
            stmt.setString(4, r.getZona_recorrido());
            stmt.setInt(5, r.getCant_recorrido());
            stmt.setBoolean(6, r.isEstado());
            stmt.setInt(7, r.getCod_recorrido());
            return stmt.executeUpdate() > 0;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            throw new MiError("No se pudo modificar el Recorrido, favor intente nuevamente");
        }
    }
    
    public boolean cambiarEstado(Recorrido r) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "update recorridos set num_bus=?, descripcion=?, tiem_demora=?, zona_recorrido=?, cant_recorrido=?, estado=?"
                    + " where cod_recorrido=?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, r.getNum_bus());
            stmt.setString(2, r.getDescripcion());
            stmt.setString(3, r.getTiem_demora());
            stmt.setString(4, r.getZona_recorrido());
            stmt.setInt(5, r.getCant_recorrido());
            r.setEstado(false);
            stmt.setBoolean(6, r.isEstado());
            stmt.setInt(7, r.getCod_recorrido());
            return stmt.executeUpdate() > 0;
        } catch (Exception ex) {
            throw new MiError("No se pudo eliminar el Recorrido, favor intente nuevamente");
        }
    }
    
    private Recorrido cargarRec(ResultSet rs) throws SQLException {
        Recorrido r = new Recorrido();
        r.setCod_recorrido(rs.getInt("cod_recorrido"));
        r.setNum_bus(rs.getInt("num_bus"));
        r.setDescripcion(rs.getString("descripcion"));
        r.setTiem_demora(rs.getString("tiem_demora"));
        r.setZona_recorrido(rs.getString("zona_recorrido"));
        r.setCant_recorrido(rs.getInt("cant_recorrido"));
        r.setEstado(rs.getBoolean("estado"));
        return r;
    }
    
    public Recorrido cargarRecorrido(int code) {
        Recorrido r = new Recorrido();
        
        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from recorridos where estado = true and cod_recorrido=?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, code);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                r = cargarRec(rs);
            }
        } catch (Exception ex) {
            throw new MiError("Problemas al cargar el recorrido, favor intente nuevamente");
        }
        return r;

    }
    
    public Recorrido cargarRecorridoN(int bus) {
        Recorrido r = null;
        
        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from recorridos where estado = true and num_bus = ? ";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, bus);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                r = cargarRec(rs);
            }
        } catch (Exception ex) {
            throw new MiError("Problemas al cargar el recorrido, favor intente nuevamente");
        }
        return r;

    }
    
}
