/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import project.entities.MiError;
import project.entities.Socio;

/**
 *
 * @author Josue
 */
public class SocioDAO {
    
    public boolean insertar(Socio s) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "insert into socios(nombre, apellido, linea_socio, num_bus, capacidad_bus, ayudante, chofer, estado) "
                    + "values (?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, s.getNombre());
            stmt.setString(2, s.getApellido());
            stmt.setString(3, s.getLinea_socio());
            stmt.setInt(4, s.getNum_bus());
            stmt.setInt(5, s.getCapacidad_bus());
            stmt.setString(6, s.getAyudante());
            stmt.setString(7, s.getChofer());
            stmt.setBoolean(8, s.isEstado());
            return stmt.executeUpdate() > 0;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            throw new MiError("No se pudo registrar el socio, favor intente nuevamente");
        }
    }
    
    public Socio cargarHorarioN(int bus) {
        Socio s = null;
        
        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from socios where estado = true and num_bus = ? ";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, bus);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                s = cargarSoc(rs);
            }
        } catch (Exception ex) {
            throw new MiError("Problemas al cargar el socio, favor intente nuevamente");
        }
        return s;

    }
    
    private Socio cargarSoc(ResultSet rs) throws SQLException {
        Socio s = new Socio();
        s.setCod_socio(rs.getInt("cod_socio"));
        s.setNombre(rs.getString("nombre"));
        s.setApellido(rs.getString("apellido"));
        s.setLinea_socio(rs.getString("linea_socio"));
        s.setNum_bus(rs.getInt("num_bus"));
        s.setCapacidad_bus(rs.getInt("capacidad_bus"));
        s.setAyudante(rs.getString("ayudante"));
        s.setChofer(rs.getString("chofer"));
        s.setEstado(rs.getBoolean("estado"));
        return s;
    }
    
}
