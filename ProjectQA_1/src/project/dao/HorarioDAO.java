/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import project.entities.Horario;
import project.entities.MiError;

/**
 *
 * @author Josue
 */
public class HorarioDAO {
    
    /**
     * Metodo que inserta un horario a la base de datos seleccionada
     * @param h objeto tipo alunmno
     * @return devuelve true si el Horario fue registrado con exito, false si no lo hace
     */

    public boolean insertar(Horario h) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "insert into horarios(num_bus, descrip_horario, salida, llegada, estado) "
                    + "values (?,?,?,?,?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, h.getNum_bus());
            stmt.setString(2, h.getDescrip_horario());
            stmt.setString(3, h.getSalida());
            stmt.setString(4, h.getLlegada());
            stmt.setBoolean(5, h.isEstado());
            return stmt.executeUpdate() > 0;
        } catch (Exception ex) {
            throw new MiError("No se pudo registrar el Horario, favor intente nuevamente");
        }
    }
    
    public boolean modificar(Horario  h) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "update horarios set num_bus=?, descrip_horario=?, salida=?, llegada=?, estado=?"
                    + " where cod_horario = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, h.getNum_bus());
            stmt.setString(2, h.getDescrip_horario());
            stmt.setString(3, h.getSalida());
            stmt.setString(4, h.getLlegada());
            stmt.setBoolean(5, h.isEstado());
            stmt.setInt(6, h.getCod_horario());
            return stmt.executeUpdate() > 0;
        } catch (Exception ex) {
            throw new MiError("No se pudo modificar el horario, favor intente nuevamente");
        }
    }

    
    public boolean cambiarEstado(Horario h) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "update horarios set num_bus=?, descrip_horario=?, salida=?, llegada=?, estado=?"
                    + " where cod_horario = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, h.getNum_bus());
            stmt.setString(2, h.getDescrip_horario());
            stmt.setString(3, h.getSalida());
            stmt.setString(4, h.getLlegada());
            h.setEstado(false);
            stmt.setBoolean(5, h.isEstado());
            stmt.setInt(6, h.getCod_horario());
            return stmt.executeUpdate() > 0;
        } catch (Exception ex) {
            throw new MiError("No se pudo eliminar el horario, favor intente nuevamente");
        }
    }
    
    private Horario cargarHor(ResultSet rs) throws SQLException {
        Horario h = new Horario();
        h.setCod_horario(rs.getInt("cod_horario"));
        h.setNum_bus(rs.getInt("num_bus"));
        h.setDescrip_horario(rs.getString("descrip_horario"));
        h.setSalida(rs.getString("salida"));
        h.setLlegada(rs.getString("llegada"));
        h.setEstado(rs.getBoolean("estado"));
        return h;
    }
    
    public Horario cargarHorario(int code) {
        Horario h = new Horario();
        
        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from horarios where estado = true and cod_horario=?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, code);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                h = cargarHor(rs);
            }
        } catch (Exception ex) {
            throw new MiError("Problemas al cargar el horario, favor intente nuevamente");
        }
        //System.out.println("este: " +h);
        return h;

    }
    
    public Horario cargarHorarioN(int bus) {
        Horario h = null;
        
        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from horarios where estado = true and num_bus = ? ";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, bus);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                h = cargarHor(rs);
            }
        } catch (Exception ex) {
            throw new MiError("Problemas al cargar el horario, favor intente nuevamente");
        }
        return h;

    }

    
}
