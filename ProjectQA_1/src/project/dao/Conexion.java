/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.dao;

import project.entities.MiError;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Josue
 */
public class Conexion {
    public static final String DRIVER = "jdbc:postgresql://";
    public static final String SERVER = "localhost:5432/";
    public static final String DB = "transport";
    public static final String USER = "postgres";
    public static final String PASS = "postgres";

    /**
     * Metodo que conecta la base de datos con el netbeans
     * @return
     * @throws SQLException 
     */
    public static Connection getConexion() throws SQLException {
        Connection conn = null;
        String url = DRIVER + SERVER + DB;
        try {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection(url, USER, PASS);
        } catch (ClassNotFoundException ex) {
            throw new MiError("Falta el driver de base de datos");
        } catch (SQLException ex) {
            throw new MiError("Problemas al realizar la conexión\n" + ex.getMessage());
        }
        return conn;
    }
    
    //public static void main(String[] args) throws SQLException{
    //    Connection n = getConexion();
    //}
//    
}
