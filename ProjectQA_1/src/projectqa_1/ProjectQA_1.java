/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectqa_1;

import project.bo.HorarioBO;
import project.bo.RecorridoBO;
import project.bo.SocioBO;
import project.dao.HorarioDAO;
import project.dao.RecorridoDAO;
import project.dao.SocioDAO;
import project.entities.Horario;
import project.entities.Recorrido;
import project.entities.Socio;

/**
 *
 * @author Josue
 */
public class ProjectQA_1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Horario h = new Horario();
        HorarioDAO hdao = new HorarioDAO();
        HorarioBO hbo = new HorarioBO(hdao);
        h.setNum_bus(1);
        h.setDescrip_horario("Es un bus color negro");
        h.setSalida("02:30");
        h.setLlegada("03:20");
        h.setEstado(true);
        //hbo.registrar(h);
        //hbo.cargarCod(1);
        //hbo.modificar(1, h);
        //hbo.cambiarEstado(1);
        //hbo.validarHora("");
        
        /*--------------------------------------------------------------------*/
        
        Recorrido r = new Recorrido();
        RecorridoDAO rdao = new RecorridoDAO();
        RecorridoBO rbo = new RecorridoBO(rdao);
        
        r.setNum_bus(3);
        r.setDescripcion("Bus de transporte escolar");
        r.setTiem_demora("2:00");
        r.setZona_recorrido("Ciudad Quesada - San Ramon");
        r.setCant_recorrido(0);
        r.setEstado(true);
        
        //rbo.registrar(r);
        //rbo.modificar(3, r);
        //rbo.cambiarEstado(2);
        //rbo.cargarCod(2);
        
        /*--------------------------------------------------------------------*/
        
        Socio s = new Socio();
        SocioDAO sdao = new SocioDAO();
        SocioBO sbo = new SocioBO(sdao);
        
        s.setNombre("Josué");
        s.setApellido("Martínez");
        s.setLinea_socio("Coopatrack");
        s.setNum_bus(1);
        s.setCapacidad_bus(54);
        s.setAyudante("David");
        s.setChofer("Gabriel");
        s.setEstado(true);
        
        sbo.registrar(s);
        
    }

}
